import pandas as pd
from datetime import datetime

def drop_columns(df):
  """
Удаление лишних столбцов из датафрейма.
  """
  df = df.drop(['followers_count', 'university', 'graduation', 'faculty', 'track_code', 'can_access_closed','is_closed', 'personal.inspired_by',
                'city.id', 'personal.langs', 'personal.langs_full', 'personal.religion', 'personal.religion_id','personal.political',
                'relation_partner.id', 'relation_partner.first_name', 'relation_partner.last_name'], axis = 1)
  return df

def remane_columns(df):
  """
Переименование стобцов

  """
  df = df.rename(columns={
    'bdate': 'Дата_рождения',
    'university_name': 'Университет',
    'faculty_name': 'Факультет',
    'home_town': 'Город_рождения',
    'relation': 'Семейное_положение',
    'sex': 'Пол',
    'first_name': 'Имя',
    'last_name': 'Фамилия',    
    'city.title': 'Город_проживания',
    'personal.alcohol': 'Отношение_к_алкоголю',
    'personal.life_main': 'Главное_в_жизни',
    'personal.people_main': 'Главное_в_людях',
    'personal.smoking': 'Отношение_к_курению',
    'education_form': 'Тип обучения',
    'education_status': 'Статус обучения'})
  return df

def rename_values(df):
  """
    Расшифровка значений

  """
  df['Пол'] = df['Пол'].replace({'1': 'Женский', '2': 'Мужской'})
  df['Семейное_положение'] = df['Семейное_положение'].replace({0: 'Не указано', 
                                                               1: 'Не женат / не замужем',
                                                               2: 'Есть друг / есть подруга',
                                                               3: 'Помолвлен / помолвлена',
                                                               4: 'Женат / замужем',
                                                               5: 'Всё сложно',
                                                               6: 'В активном поиске',
                                                               7: 'Влюблён / влюблена',
                                                               8: 'В гражданском браке'})
  df['Отношение_к_алкоголю'] = df['Отношение_к_алкоголю'].replace({1: 'резко негативное',
                                                               2: 'негативное',
                                                               3: 'компромиссное',
                                                               4: 'нейтральное',
                                                               5: 'положительное'})  
  df['Отношение_к_курению'] = df['Отношение_к_курению'].replace({1: 'резко негативное',
                                                               2: 'негативное',
                                                               3: 'компромиссное',
                                                               4: 'нейтральное',
                                                               5: 'положительное'}) 
  df['Главное_в_жизни'] = df['Главное_в_жизни'].replace({1: 'семья и дети',
                                                               2: 'карьера и деньги',
                                                               3: 'развлечения и отдых',
                                                               4: 'наука и исследования',
                                                               5: 'совершенствование мира',
                                                               6: 'саморазвитие',
                                                               7: 'красота и искусство',
                                                               8: 'слава и влияние'})   
  df['Главное_в_людях'] = df['Главное_в_людях'].replace({1: 'ум и креативность',
                                                               2: 'доброта и честность',
                                                               3: 'красота и здоровье',
                                                               4: 'власть и богатство',
                                                               5: 'смелость и упорство',
                                                               6: 'юмор и жизнелюбие'})   
  return df
       
def drop_sity(df):
  """
Исключаем города случанйо попавшие в выборку
  """
  df = df[df['Город_проживания'] == 'Омск']
  return df


